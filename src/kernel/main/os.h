//=========================================================================//
// Name: os.h
// Purpose: Defines a number of OS globals
// Authors: Jeremy L.
// Version: 0.0.1-alpha
// License:
//=========================================================================//
#pragma once

//REMEMBER TO REMOVE
#define _KERNEL_BUILD_

#ifdef _KERNEL_BUILD_
#include "../kernel.h"
#include "memory.h"
#endif

//=========================================================================//
// Name: stdcolor.h
// Purpose: Defines a non-standard collection of structures and interfaces
// used to manipulate colors 
// Authors: Jeremy L.
// Version: 0.0.1-alpha
// License:
//=========================================================================//

#pragma once